/* Style Changer */


jQuery(document).ready(function(){

	/* Background Scheme */
	jQuery('#stlChanger .stBgs a').click(function () {
		jQuery('#stlChanger .stBgs a.current').removeClass('current');
		jQuery('.header_inner').removeAttr('style');
		jQuery(this).addClass('current');
		color1 = jQuery(this).find('.color1').attr('title');
		color2 = jQuery(this).find('.color2').attr('title');
		color3 = jQuery(this).find('.color3').attr('title');
		color4 = jQuery(this).find('.color4').attr('title');
		color5 = jQuery(this).find('.color5').attr('title');
		jQuery('#color_scheme').text('#slide_top:hover, .social_but, h2:before, .headline:after, .header_inner:after, .button:hover, .pricing_button:hover, .button_small:hover, .button_medium:hover, .button_large:hover, input[type="submit"]:hover, .cmsmsLike:hover, .cmsmsLike.active, .cmsmsLike:hover span, .cmsmsLike.active span, .tog:hover:before, .tog.current:before, ul.page-numbers span, .cmsms_content_slider_parent ul.cmsms_slides_nav li a, .tp-bullets.simplebullets.round .bullet:hover, .tp-bullets.simplebullets.round .bullet.selected, .ls-industrial .ls-bottom-slidebuttons a.ls-nav-active,.ls-industrial .ls-bottom-slidebuttons a:hover, .pj_sort a[name="pj_name"]:after, .pj_sort a[name="pj_date"]:after, a.pj_cat_filter:after, .cmsms_share:hover:before, .commentlist li div.comment-content ul li:hover:before, div.jp-playlist li:hover:before, .list li:hover:before, .widget_links ul li:hover:before, .widget.widget_archive ul li:hover:before, .widget.widget_categories ul li:hover:before, .widget.widget_meta ul li:hover:before, .widget.widget_recent_comments ul li:hover:before, .widget.widget_recent_entries ul li:hover:before, .widget.widget_pages ul li:hover:before, .widget.widget_nav_menu ul li:hover:before, .content_wrap div[data-folder="text"] ul li:hover:before, .rev_slider > ul > li a.more_but:hover:before, a.more_button:hover:before {background-color:#' + color1 + ';} .tp-caption.cmsms_large_text {border-left-color:#' + color1 + ';} #navigation > li.current_page_item > a, #navigation > li.current-menu-ancestor > a, #navigation > li > ul:before{border-bottom-color:#' + color1 + ';} .pj_filter_container ul.pj_filter_list, #navigation li > ul, code {border-top-color:#' + color1 + ';} .color_3, q:before, .cmsms_meta_inner h5, blockquote:before, .comment-edit-link, .comment-reply-link, #cancel-comment-reply-link, .widget_custom_recent_testimonials_entries .tl_author a, .related_posts_content a:hover, .table tr th, .cmsms_icon_title:hover, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a, .project_rollover .entry-header .entry-title a:hover, a.more_button:hover, #navigation > li > a {color:#' + color1 + ';} a, .person_subtitle, .comment-body .published, .tp-caption a {color:#' + color2 + ';}' );
	});
	
	
	if (jQuery(window).height() < 550) {
		jQuery('#stlChanger').css( { 
			position : 'absolute' 
		} );
	}
	
	
	/* Style Changer Toggle */
	jQuery('.chBut').click(function(){
		if (jQuery(this).hasClass('closed')){
			jQuery(this).next('.chBody').css({display:'block'}).parent().animate({left:0}, 500, function(){
				jQuery(this).find('.chBut').removeClass('closed');
			});
		} else {
			jQuery(this).parent().animate({left:'-103px'}, 500, function(){
				jQuery(this).find('.chBut').next('.chBody').css({display:'none'});
				jQuery(this).find('.chBut').addClass('closed');
			});
		}
		
		return false;
	});
	
	
	/* Window Resize Function */
	jQuery(window).resize(function(){
		if (jQuery(window).height() < 750){
			jQuery('#stlChanger').css({position:'absolute'});
		} else {
			jQuery('#stlChanger').css({position:'fixed'});
		}
	});
	
});
