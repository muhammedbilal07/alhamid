<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<section id="top">
        <div class="wrap_rev_slider"> 
          
          <!-- START REVOLUTION SLIDER 3.0.5 fullwidth mode -->
          
          <link rel='stylesheet' id='rev-google-font' href='http://fonts.googleapis.com/css?family=Raleway:400,700' type='text/css' media='all' />
          <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:552px;">
            <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;max-height:552px;height:552;">
              <ul>
                <li data-transition="boxfade" data-slotamount="7" data-masterspeed="700" > <img src="wp-content/uploads/2014/01/s11.jpg"  alt="1"  data-fullwidthcentering="on">
                  
                </li>
                <li data-transition="boxfade" data-slotamount="7" data-masterspeed="300" > <img src="wp-content/uploads/2014/01/s31.jpg"  alt="2"  data-fullwidthcentering="on">
                  
                </li>
                <li data-transition="boxfade" data-slotamount="7" data-masterspeed="300" > <img src="wp-content/uploads/2014/01/s21.jpg"  alt="3"  data-fullwidthcentering="on">
                  
                </li>
                <li data-transition="boxfade" data-slotamount="7" data-masterspeed="300" > <img src="wp-content/uploads/2014/01/s6.jpg"  alt="4"  data-fullwidthcentering="on">
                  
                </li>
              </ul>
            </div>
          </div>
          <script type="text/javascript">

				var tpj=jQuery;
				
									tpj.noConflict();
								
				var revapi1;
				
				tpj(document).ready(function() {
				
				if (tpj.fn.cssOriginal != undefined)
					tpj.fn.css = tpj.fn.cssOriginal;
				
				if(tpj('#rev_slider_1_1').revolution == undefined)
					revslider_showDoubleJqueryError('#rev_slider_1_1');
				else
				   revapi1 = tpj('#rev_slider_1_1').show().revolution(
					{
						delay:9000,
						startwidth:1160,
						startheight:552,
						hideThumbs:200,
						
						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:4,
						
						navigationType:"bullet",
						navigationArrows:"solo",
						navigationStyle:"round",
						
						touchenabled:"on",
						onHoverStop:"on",
						
						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:20,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,
								
						shadow:0,
						fullWidth:"on",
						fullScreen:"off",

						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off",
						
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						startWithSlide:0,
						videoJsPath:"http://industrial.cmsmasters.net/wp-content/plugins/revslider/rs-plugin/videojs/",
						fullScreenOffsetContainer: ""	
					});
				
				});	//ready
				
			</script> 
          
          <!-- END REVOLUTION SLIDER -->
          
          <div class="cl"></div>
        </div>
      </section>
      <!-- __________________________________________________ Finish Top -->
      <div class="content_wrap fullwidth"> 
        
        <!--_________________________ Start Content _________________________ -->
        <section id="middle_content" role="main">
          <div class="entry">
            <div class="cmsms_cc">
              <div class="one_first first_column" data-folder="column" data-type="">
                <div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
              </div>
              <div class="one_first first_column" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h1><span style="font-size: 40px;">SuperQuality<span style="font-weight: 400;">Only</span></span></h1>
                </div>
              </div>
              <div class="one_first first_column" data-folder="column" data-type="">
                <div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
              </div>
              <div class="one_fourth first_column" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h2 style="text-align: left;"><strong>01</strong> Pipe/Tubes</h2>
                  <p>Custom admin panel significantly extends original Wordpress functionality, turning it into an all-in-one tool for content management.</p>
                  <p><a class="more_button" href="http://alhamidcorporation.com/products/">Read more</a></p>
                </div>
              </div>
              <div class="one_fourth" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h2 style="text-align: left;"><strong>02</strong> Fittings</h2>
                  <p>A real-time WYSIWYG editor: you are able to customize the look of your website directly on page and see your changes right away - what you see is what you get!</p>
                  <p><a class="more_button" href="http://alhamidcorporation.com/products/">Read more</a></p>
                </div>
              </div>
              <div class="one_fourth" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h2 style="text-align: left;"><strong>03</strong> Valves</h2>
                  <p>Theme offers super flexibility for color editing and easily turns from a minimalistic style theme into a juicy website with great animation effects.</p>
                  <p><a class="more_button" href="http://alhamidcorporation.com/products/">Read more</a></p>
                </div>
              </div>
              <div class="one_fourth" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h2 style="text-align: left;"><strong>04</strong> Flanges</h2>
                  <p>Visual editor allows you also save ready shortcodes as templates and use them on other pages, what saves you a lot of time and effort.</p>
                  <p><a class="more_button" href="http://alhamidcorporation.com/products/">Read more</a></p>
                </div>
              </div>
              <div class="one_first first_column" data-folder="divider" data-type="clear">
                <div class="cl"></div>
              </div>
              <div class="one_first first_column" data-folder="column" data-type="">
                <div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
              </div>
              <div class="one_first first_column" data-folder="column" data-type="">
                <div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
              </div>
              <div class="one_half first_column" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h2>Products We Deal In</h2>
                  <br>
                </div>
                <div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
                <div data-folder="text" data-type=""><img class="size-thumbnail wp-image-5532 alignleft max_width" alt="industrial (40)" src="wp-content/uploads/2014/01/fuc1.jpg" height="127" width="127">
                  <h5 class="color_3">We assures you 100% Quality Products</h5>
We have build long term relationships /strategies alliance with our overseas suppliers who never make compromise on quality, rather improve quality every second of time.Our Carbon Steel Seamless Pipe is unique from others (available in market). Our core band BAOSTEEL provides excellent Seamless experience to our valuable customers through its physical & chemical composition, exact weight, wall thickness, every aspect up to the mark.Our all products are certified from the high ranked international quality organization. We get material inspection /mill test certificate of products which assure 100% quality. Here we are pleased to share our some vendors of different products.
 <br />
                      <b>1. Ebro armaturen – Germany(Butterfly valves)</b><br />
                      <b>2. Bao steel Singapore PTE ltd(seamless pipe)</b><br />
                      <b>3. Bao shan iron & Steel( Boiler tubes)</b><br />
                      <b>4. Showa valves company LTD,- Japan</b><br />
                      <b>5. Both well – Taiwan etc.(High pressure forged steel fittings)</b><br />
                    
                  
</div>
                <div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
                <div data-folder="text" data-type="">
                  <h2>Whole-Seller & Retailer</h2>
<img class="size-thumbnail wp-image-5639 alignleft" alt="person1" src="wp-content/uploads/2014/01/fuc2.jpg" height="127" width="127">
Our target market is to entertain projects of all kinds of industries, sugar mills, textile mills,Cement,etc and regular maintenance of Industry.                   
</div>
              </div>
              <div class="one_half" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h2>Our Advantages</h2>
                </div>
                <div data-folder="text" data-type="">
                 Al-Hamid is a team of professionals, who are technically & managerially mature to meet with the complex specification efficiently. Our vast experience allow our customers to get consultancy in technical matters. Every member of our team has spent many hours in training & development of skills to cope with different industry segments. We are always available to serve our valuable customers. You can Contact us anytime.
                </div>
                <div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
                <div data-folder="text" data-type="">
                  <p>&nbsp;</p>
                 <h2>Technologies</h2>
		</div>

<div data-folder="text" data-type="">
                    <b><ol>
                      <li>High technologies in Valves(Stainless seat, bellow seat etc)</li>
                      <li>High pressure pipes in all Schedules and thickness SCH-20, SCH-40, SCH-80, SCH-160 so on…</li>
                      <li>High pressure boiler tubes</li>
                    </ol></b>
<div align="center">
<img src="wp-content/themes/alhamid/images/1.jpg" align="left" alt="logo" width="35%"><img src="wp-content/themes/alhamid/images/2.png" alt="logo" align="right" width="50%" >
		<div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
                <br />

<img src="wp-content/themes/alhamid/images/5.jpg" align="left" alt="logo"><img src="wp-content/themes/alhamid/images/4.jpg" alt="logo" align="right">
		<div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
                <br />
<img src="wp-content/themes/alhamid/images/3.jpg" align="left" alt="logo" width="40%"><img src="wp-content/themes/alhamid/images/6.jpg" alt="logo" align="right">
		<div data-folder="divider" data-type="clear">
                  <div class="cl"></div>
                </div>
                <br />
</div>
</div>
                  </div>


 
              </div>
<h2>NOTE</h2>
* <strong>All material is available in stock</strong><br />
* <strong>All deliveries are handled in short time.</strong><br />
            </div>
          </div>
        </section>
        <!-- _________________________ Finish Content _________________________ -->
        
        <div class="cl"></div>
      </div>
    </section>
    <!-- _________________________ Finish Middle _________________________ -->
    
   <!-- <div class="cmsms_wrap_latest_bottom_tweets">
      <div id="cmsms_latest_bottom_tweets">
        <ul class="jta-tweet-list responsiveContentSlider">
          <li class="jta-tweet-list-item">Agriculture Wordpress Theme  by @cmsmasters on @dribbble <a href="http://t.co/8GauyYPnTn" target="_blank" rel="nofollow">http://t.co/8GauyYPnTn</a></li>
          <li class="jta-tweet-list-item">Dream Admin  - Agriculture theme by @cmsmasters on @dribbble <a href="http://t.co/we171r75ZU" target="_blank" rel="nofollow">http://t.co/we171r75ZU</a></li>
          <li class="jta-tweet-list-item">Agriculture Theme by @cmsmasters on @dribbble <a href="http://t.co/hTgyyV2Jmi" target="_blank" rel="nofollow">http://t.co/hTgyyV2Jmi</a></li>
          <li class="jta-tweet-list-item">Agriculture WP Theme by @cmsmasters on @dribbble <a href="http://t.co/bbCT0BgNHW" target="_blank" rel="nofollow">http://t.co/bbCT0BgNHW</a></li>
          <li class="jta-tweet-list-item">Agriculture Wordpress Theme by @cmsmasters on @dribbble <a href="http://t.co/nMjQzrOI9g" target="_blank" rel="nofollow">http://t.co/nMjQzrOI9g</a></li>
          <li class="jta-tweet-list-item">Agriculture WooCommerce Theme by @cmsmasters on @dribbble <a href="http://t.co/W2C8mZc5o5" target="_blank" rel="nofollow">http://t.co/W2C8mZc5o5</a></li>
        </ul>
      </div>
    </div> -->
    <script type="text/javascript">
		jQuery(document).ready(function () { 
			jQuery('#cmsms_latest_bottom_tweets .jta-tweet-list').cmsmsResponsiveContentSlider( {
				sliderWidth : '100%',
				sliderHeight : 'auto',
				animationSpeed : 500,
				animationEffect : 'fade',
				animationEasing : 'easeInOutExpo',
				pauseTime : 7000,
				activeSlide : 1, 
				touchControls : true,
				pauseOnHover : false, 
				arrowNavigation : true, 
				slidesNavigation : false 
			} );
		} );
	</script> 

<?php get_sidebar(); ?>
<?php get_footer(); ?>