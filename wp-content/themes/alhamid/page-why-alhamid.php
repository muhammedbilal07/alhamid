<?php
/*
Template Name: Content Page
*/
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	 <!-- <div class="cmsms_fullwidth_thumb" style=""></div> -->
      <div class="headline cmsms-with-parallax">
        <div>
         
        </div>
      </div>
      <div class="content_wrap fullwidth"> 
        
        <!--_________________________ Start Content _________________________ -->
        <section id="middle_content" role="main">
          <div class="entry">
          	<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

			
			<?php endwhile; ?>
            <!--<div class="cmsms_cc">
              <div class="three_fourth first_column" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <p><img class="size-full wp-image-5892 alignleft max_width" alt="responsive" src="../wp-content/uploads/2013/08/responsive.png" height="552" width="837"></p>
                </div>
              </div>
              <div class="one_fourth" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                
                  <h2>PROFILE</h2>
                  <p dir="ltr">A 100% Responsive, with Retina ready, Industrial Theme will have a great appearance on a large resolution desktop screens, as well as on various mobile devices' screens, including tablets, cellphones and other. All contemporary browsers are supported either!</p>
                </div>
              </div>
              <div class="one_first first_column" data-folder="divider" data-type="divider">
                <div class="divider"></div>
              </div>
              <div class="one_fourth first_column" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h3>&nbsp;</h3>
                  <h3>&nbsp;</h3>
                  <p>&nbsp;</p>
                  <h2 dir="ltr">Many Shortcodes</h2>
                  <p dir="ltr">Insdustrial theme for wordpress custom admin panel includes a Super Extended Shortcodes pack for sophisticated content formatting and improved presentation. A wide collection of useful widgets allow you improve your website’s interaction with visitors, bringing its functionality to a new level.</p>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="three_fourth" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <p><img class="size-full wp-image-5891 alignright max_width" alt="Extended-shortcodes-pack" src="../wp-content/uploads/2013/08/Extended-shortcodes-pack.png" height="614" width="760"></p>
                </div>
              </div>
              <div class="one_first first_column" data-folder="divider" data-type="divider">
                <div class="divider"></div>
              </div>
              <div class="three_fourth first_column" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <p style="text-align: center;"><img class="size-full wp-image-5893 aligncenter max_width" alt="seo" src="../wp-content/uploads/2013/08/seo.png" height="546" width="760"></p>
                </div>
              </div>
              <div class="one_fourth" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h3>&nbsp;</h3>
                  <h3>&nbsp;</h3>
                  <p>&nbsp;</p>
                  <h2 dir="ltr">Advanced SEO</h2>
                  <p dir="ltr">Industrial has a really clean CSS3 and HTML5 code that the search engines will love! Due to the optimized code and structure, your creative agency website will be easily crawled and successfully indexed by search engines. Not only that, the theme also provides custom breadcrumbs, tags, descriptions and SEO titles formatting, and easy sitemap, which all works great for your SEO performance.</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="one_first first_column" data-folder="divider" data-type="divider">
                <div class="divider"></div>
              </div>
              <div class="one_fourth first_column" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h3>&nbsp;</h3>
                  <h2 dir="ltr">Visual Content Editor</h2>
                  <p dir="ltr">The drag’n’drop content composer allows you easily rearrange your content parts simply dragging them through the page. &nbsp;The Visual editor allows you also save your ready shortcodes as templates and use them with the same settings on other pages, what saves you a lot of time for website management. &nbsp;You can easily create a new custom page simply by adding several of your pre-saved templates and the rearranging them with drag’n’drop on your page and easily resizing their width on just a couple of clicks!</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="three_fourth" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <p style="text-align: center;"><img class="size-full wp-image-5894 aligncenter max_width" alt="visual-content-composer" src="../wp-content/uploads/2013/08/visual-content-composer.png" height="470" width="760"></p>
                </div>
              </div>
              <div class="one_first first_column" data-folder="divider" data-type="divider">
                <div class="divider"></div>
              </div>
              <div class="three_fourth first_column" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <p style="text-align: center;"><img class="size-full wp-image-5896 aligncenter max_width" alt="contact-form" src="../wp-content/uploads/2013/08/contact-form1.png" height="473" width="760"></p>
                </div>
              </div>
              <div class="one_fourth" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h3>&nbsp;</h3>
                  <h2 dir="ltr">Form Builder</h2>
                  <p dir="ltr">Powerful Form Builder lets you create a really interactive website easily. With this form Builder you can make not only great contact &nbsp;forms, but as well wonderful questionnaires and polls to involve your website visitors into a discussion and collect the data you need. These forms and questionnaires can be located on any place of your page, &nbsp;including content area or sidebars, as well as resized to a certain width.</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="one_first first_column" data-folder="divider" data-type="divider">
                <div class="divider"></div>
              </div>
              <div class="one_fourth first_column" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <h3>&nbsp;</h3>
                  <h2 dir="ltr">Support &amp; Docs</h2>
                  <p dir="ltr">We care not only to create quality themes, but as well make sure our customers succeed to setup a fantastic website no matter whether being a professional or an amateur Wordpress user. This is why we are keep to providing you with the most helpful support and assistance. Besides, this, we supply all our themes with the most detailed documentation, so that precisely any possible question you may encounter is thoroughly covered and explained.</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="three_fourth" data-folder="column" data-type="">
                <div data-folder="text" data-type="">
                  <p style="text-align: center;"><img class="size-full wp-image-5890 aligncenter max_width" alt="documentation" src="../wp-content/uploads/2013/08/documentation.png" height="432" width="760"></p>
                </div>
              </div>
            </div>-->
          </div>
        </section>
        <!-- _________________________ Finish Content _________________________ -->
        
        <div class="cl"></div>
      </div>
    </section>
    <!-- _________________________ Finish Middle _________________________ 
    
    <div class="cmsms_wrap_latest_bottom_tweets">
      <div id="cmsms_latest_bottom_tweets">
        <ul class="jta-tweet-list responsiveContentSlider">
          <li class="jta-tweet-list-item">Agriculture Wordpress Theme  by @cmsmasters on @dribbble <a href="http://t.co/8GauyYPnTn" target="_blank" rel="nofollow">http://t.co/8GauyYPnTn</a></li>
          <li class="jta-tweet-list-item">Dream Admin  - Agriculture theme by @cmsmasters on @dribbble <a href="http://t.co/we171r75ZU" target="_blank" rel="nofollow">http://t.co/we171r75ZU</a></li>
          <li class="jta-tweet-list-item">Agriculture Theme by @cmsmasters on @dribbble <a href="http://t.co/hTgyyV2Jmi" target="_blank" rel="nofollow">http://t.co/hTgyyV2Jmi</a></li>
          <li class="jta-tweet-list-item">Agriculture WP Theme by @cmsmasters on @dribbble <a href="http://t.co/bbCT0BgNHW" target="_blank" rel="nofollow">http://t.co/bbCT0BgNHW</a></li>
          <li class="jta-tweet-list-item">Agriculture Wordpress Theme by @cmsmasters on @dribbble <a href="http://t.co/nMjQzrOI9g" target="_blank" rel="nofollow">http://t.co/nMjQzrOI9g</a></li>
          <li class="jta-tweet-list-item">Agriculture WooCommerce Theme by @cmsmasters on @dribbble <a href="http://t.co/W2C8mZc5o5" target="_blank" rel="nofollow">http://t.co/W2C8mZc5o5</a></li>
        </ul>
      </div>
    </div> -->
    <script type="text/javascript">
		jQuery(document).ready(function () { 
			jQuery('#cmsms_latest_bottom_tweets .jta-tweet-list').cmsmsResponsiveContentSlider( {
				sliderWidth : '100%',
				sliderHeight : 'auto',
				animationSpeed : 500,
				animationEffect : 'fade',
				animationEasing : 'easeInOutExpo',
				pauseTime : 7000,
				activeSlide : 1, 
				touchControls : true,
				pauseOnHover : false, 
				arrowNavigation : true, 
				slidesNavigation : false 
			} );
		} );
	</script> 
<?php get_footer(); ?>