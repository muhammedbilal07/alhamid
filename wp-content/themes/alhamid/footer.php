<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
 <!-- _________________________ Start Bottom _________________________ -->
    <section id="bottom">
      <div class="bottom_inner">
        <!--<div class="bottom_content">
          <div class="one_fourth">
            <aside id="recent-posts-3" class="widget widget_recent_entries">
              <h5 class="widgettitle">Recent Posts</h5>
              <ul>
                <li> <a href="standard-post-with-parallax-heading/index.html" title="Standard Post With Parallax Heading">Standard Post With Parallax Heading</a> </li>
                <li> <a href="standard-post-with-an-image/index.html" title="Standard Post With an Image">Standard Post With an Image</a> </li>
                <li> <a href="quote-post-format/index.html" title="Quote post format">Quote post format</a> </li>
                <li> <a href="sandard-post-without-image/index.html" title="Standard Post Without Image">Standard Post Without Image</a> </li>
                <li> <a href="396/index.html" title="Video post format">Video post format</a> </li>
                <li> <a href="gallery-post-format/index.html" title="Gallery Post Format">Gallery Post Format</a> </li>
              </ul>
            </aside>
          </div>
          <div class="one_fourth">
            <aside id="recent-comments-3" class="widget widget_recent_comments">
              <h5 class="widgettitle">Recent Comments</h5>
              <ul id="recentcomments">
                <li class="recentcomments">Keith on <a href="project/single-image/index.html#comment-4">Single Image</a></li>
                <li class="recentcomments">Fred on <a href="project/project/index.html#comment-5">Project</a></li>
                <li class="recentcomments">Caroline on <a href="project/multiple-media/index.html#comment-6">Multiple Media</a></li>
                <li class="recentcomments">Jack on <a href="gallery-post-format/index.html#comment-9">Gallery Post Format</a></li>
                <li class="recentcomments">Gabi on <a href="gallery-post-format/index.html#comment-8">Gallery Post Format</a></li>
              </ul>
            </aside>
          </div>
          <div class="cl_resp"></div>
          <div class="one_fourth">
            <aside id="categories-3" class="widget widget_categories">
              <h5 class="widgettitle">Categories</h5>
              <ul>
                <li class="cat-item cat-item-2"><a href="category/image/index.html" title="View all posts filed under Image">Image</a> </li>
                <li class="cat-item cat-item-3"><a href="category/posts-with-audio/index.html" title="View all posts filed under Posts with audio">Posts with audio</a> </li>
                <li class="cat-item cat-item-4"><a href="category/posts-with-image/index.html" title="View all posts filed under Posts with image">Posts with image</a> </li>
                <li class="cat-item cat-item-5"><a href="category/posts-with-video/index.html" title="View all posts filed under Posts with video">Posts with video</a> </li>
                <li class="cat-item cat-item-6"><a href="category/standard/index.html" title="View all posts filed under standard">standard</a> </li>
                <li class="cat-item cat-item-7"><a href="category/text-posts/index.html" title="View all posts filed under Text posts">Text posts</a> </li>
                <li class="cat-item cat-item-1"><a href="category/uncategorized/index.html" title="View all posts filed under Uncategorized">Uncategorized</a> </li>
              </ul>
            </aside>
          </div>
          <div class="one_fourth">
            <aside id="meta-3" class="widget widget_meta">
              <h5 class="widgettitle">Meta</h5>
              <ul>
                <li><a href="wp-login.html">Log in</a></li>
                <li><a href="feed/index.html" title="Syndicate this site using RSS 2.0">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
                <li><a href="comments/feed/index.html" title="The latest comments to all posts in RSS">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
                <li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li>
              </ul>
            </aside>
          </div>
        </div>-->
      </div>
    </section>
    <!-- _________________________ Finish Bottom _________________________ --> 
    
    <a href="javascript:void(0);" id="slide_top"></a> </div>
  <!-- _________________________ Finish Container _________________________ --> 
  
  <!-- _________________________ Start Footer _________________________ -->
  <footer id="footer" role="contentinfo">
    <div class="footer_inner"> <span class="copyright">Al-Hamid Corporation (Pvt) Limited © 2014</span> </div>
  </footer>
  <!-- _________________________ Finish Footer _________________________ --> 
  
</section>
<!-- _________________________ Finish Page _________________________ --> 

<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery('.cmsms_social').socicons( {
			icons : 'nujij,ekudos,digg,linkedin,sphere,technorati,delicious,furl,netscape,yahoo,google,newsvine,reddit,blogmarks,magnolia,live,tailrank,facebook,twitter,stumbleupon,bligg,symbaloo,misterwong,buzz,myspace,mail,googleplus',
			imagesurl : 'http://industrial.cmsmasters.net/<?php echo get_template_directory_uri(); ?>/img/share_icons/'
		} );
	} );
</script> 
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery.easing.min6f3e.js?ver=1.3.0'></script> 
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jackbox-lib8a54.js?ver=1.0.0'></script> 
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jackbox8a54.js?ver=1.0.0'></script> 
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery.script8a54.js?ver=1.0.0'></script> 
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery.jPlayer.min3c94.js?ver=2.1.0'></script> 
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery.jPlayer.playlist.min8a54.js?ver=1.0.0'></script> 
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery.tweet.mine7f0.js?ver=1.3.1'></script>
<style id="color_scheme" type="text/css">
</style>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/styleChanger/colorpicker/colorpicker.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/styleChanger/changer.js"></script>
<script type="text/javascript">
$(".page-item-13").children('a').click(function(e){
  e.preventDefault()
})
</script>
<?php wp_footer(); ?>
</body>

<!-- Mirrored from industrial.cmsmasters.net/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 25 Oct 2013 17:47:00 GMT -->
</html>
