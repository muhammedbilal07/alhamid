<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
    <meta name="description" content="Industrial Wordpress Theme by cmsmasters" />
	<meta name="keywords" content="Industrial" />
    <link rel="shortcut icon" href="wp-content/uploads/2013/08/favicon.jpg" type="image/x-icon" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="xmlrpc.php" />
    
    <link rel="alternate" type="application/rss+xml" title="Industrial &raquo; Feed" href="feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Industrial &raquo; Comments Feed" href="comments/feed/index.html" />
    <link rel='stylesheet' id='layerslider_css-css'  href='<?php echo get_template_directory_uri(); ?>/css/layerslider2cb6.css?ver=4.5.5' type='text/css' media='all' />
  
    <link rel='stylesheet' id='theme-style-css'  href='<?php echo get_template_directory_uri(); ?>/style.css?time=<?php echo time()?>' type='text/css' media='screen' />
    <link rel='stylesheet' id='theme-fonts-css'  href='<?php echo get_template_directory_uri(); ?>/css/fonts8a54.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='theme-adapt-css'  href='<?php echo get_template_directory_uri(); ?>/css/adaptive8a54.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='theme-retina-css'  href='<?php echo get_template_directory_uri(); ?>/css/retina8a54.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='jackbox-css'  href='<?php echo get_template_directory_uri(); ?>/css/jackbox8a54.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='jPlayer-css'  href='<?php echo get_template_directory_uri(); ?>/css/jquery.jPlayer3c94.css?ver=2.1.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='isotope-css'  href='<?php echo get_template_directory_uri(); ?>/css/jquery.isotope5d6b.css?ver=1.5.19' type='text/css' media='screen' />
    <!--[if lt IE 9]>
    <link rel='stylesheet' id='theme-ie-css'  href='http://industrial.cmsmasters.net/<?php echo get_template_directory_uri(); ?>/css/ie.css?ver=1.0.0' type='text/css' media='screen' />
    <![endif]-->
    <!--[if lt IE 9]>
    <link rel='stylesheet' id='theme-ieCss3-css'  href='http://industrial.cmsmasters.net/<?php echo get_template_directory_uri(); ?>/css/ieCss3.php?ver=1.0.0' type='text/css' media='screen' />
    <![endif]-->
    <link rel='stylesheet' id='cmsms-google-font-1-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A200%2C400%2C600%2C700&amp;ver=3.6' type='text/css' media='all' />
    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.all.min16b9.js?ver=2.5.2'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/respond.minf488.js?ver=1.1.0'></script>
   <!-- <script type='text/javascript' src='wp-includes/js/jquery/jquery3e5a.js?ver=1.10.2'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min1576.js?ver=1.2.1'></script>
    <script type='text/javascript' src='wp-includes/js/comment-reply.min039c.js?ver=3.6'></script>-->
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/layerslider.kreaturamedia.jquery2cb6.js?ver=4.5.5'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery-easing-1.36f3e.js?ver=1.3.0'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquerytransite2dc.js?ver=0.9.9'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/layerslider.transitions2cb6.js?ver=4.5.5'></script>
    <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery.themepunch.revolution.min039c.js?ver=3.6'></script>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" /> 
    <link rel='prev' title='Testimonials' href='testimonials/index.html' />
    <link rel='next' title='Featured Blocks' href='featured-blocks/index.html' />
    <meta name="generator" content="WordPress 3.6" />
    <link rel='canonical' href='index.html' />
    <script type="text/javascript">var templateURL = "<?php echo get_template_directory_uri(); ?>.html";</script><style type="text/css">body { background-color : #ffffff; background-image : none; background-position : top center; background-repeat : repeat; background-attachment : scroll; }#middle.cmsms_middle_margin { margin-top : 150px; } #header > .header_inner { height : 150px; } #header.fixed_header + * { margin-top : 150px; } #header > .header_inner > a.logo { top : 35px; left : 0px; } .header_inner:after { width : 255px; } #header > .header_inner > a.logo > img { width : 120px; height : 70px; } #header nav { top : 45px; left : 255px; } #header .social_icons { top : 50px; left : 255px; } #header .search_line { top : 75px; left : 255px; } .header_html { top : 15px; right : 0px; } #header.fixed_header > .header_inner { height : 100px; } #header.fixed_header > .header_inner > a.logo { top : 10px; } #header.fixed_header nav { top : 20px; } #header.fixed_header .social_icons { top : 25px; } #header.fixed_header .search_line { top : 50px; } @media only screen and (max-width : 1024px) { #middle.cmsms_middle_margin { margin-top : 0px; } .header_inner:after { margin-left : 5%; } #header nav { top : auto; left : auto; } #header > .header_inner > a.logo { top : auto; left : auto; } #header > .header_inner { height : auto; } } @media only screen and (max-width : 767px) { #header .search_line, #header .social_icons { top : auto; left : auto; } } @media only screen and (max-width : 450px) { .header_html { top : auto; } #header > .header_inner > a.logo { width : 90%; height : auto; } #header > .header_inner > a.logo > img { margin : 0 auto; } .header_inner:after { width : 90%; } } </style>	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <style type="text/css">
            .recentcomments a {
                background:none !important;
                display:inline !important;
                padding:0 !important;
                margin:0 !important;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styleChanger/changer.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styleChanger/colorpicker/colorpicker.css" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<!-- _________________________ Start Page _________________________ -->
            <section id="page" class="csstransition hfeed site"> <a href="#" id="slide_top"></a> 
              <!-- _________________________ Start Container _________________________ -->
              <div class="container set_fixed"> 
                
                <!-- _________________________ Start Header _________________________ -->
                 <header class="
                     header_position" id="header">
                      <div class="header_inner"> <a href="<?php echo site_url()?>" title="Industrial" class="logo"> 
<?php $upload_path=wp_upload_dir(); ?><img src="<?php echo site_url()?>/wp-content/uploads/2014/01/logo-190x107.png" alt="Al-Hamid Corp." /> <a class="responsive_nav" href="javascript:void(0);"></a> <!-- _________________________ Start Navigation _________________________ -->
                        <nav role="navigation" class=" nav_numbering">
                       
                          <ul id="navigation" class="navigation">
                          <li id="menu-item-5221" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-5221"><a href="<?php echo site_url()?>"><span>Home</span></a></li>
                          <?php wp_list_pages('title_li='); ?> 
<!--                            <li id="menu-item-5221" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-5221"><a href="#"><span>Home</span></a>
                              <ul class="sub-menu">
                                <li id="menu-item-5437" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2358 current_page_item menu-item-5437"><a href="index.html"><span>Home Page 1</span></a></li>
                                <li id="menu-item-5775" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5775"><a href="home-2/index.html"><span>Home Page 2</span></a></li>
                                <li id="menu-item-5776" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5776"><a href="home-3-2/index.html"><span>Home Page 3</span></a></li>
                              </ul>
                            </li>
                            <li id="menu-item-5216" class="dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-5216"><a><span>Features</span></a>
                              <ul class="sub-menu">
                                <li id="menu-item-5217" class="dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-5217"><a><span>Shortcodes</span></a>
                                  <ul class="sub-menu">
                                    <li id="menu-item-5222" class="dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-5222"><a><span>Block Shortcodes</span></a>
                                      <ul class="sub-menu">
                                        <li id="menu-item-5473" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5473"><a href="person-block/index.html"><span>Person Block</span></a></li>
                                        <li id="menu-item-5443" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5443"><a href="featured-blocks/index.html"><span>Featured Blocks</span></a></li>
                                        <li id="menu-item-5442" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5442"><a href="colored-blocks/index.html"><span>Colored Blocks</span></a></li>
                                        <li id="menu-item-5446" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5446"><a href="columns/index.html"><span>Columns</span></a></li>
                                        <li id="menu-item-5453" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5453"><a href="tabs-and-toggle/index.html"><span>Tabs &#038; Toggle</span></a></li>
                                        <li id="menu-item-5450" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5450"><a href="information-boxes/index.html"><span>Information Boxes</span></a></li>
                                        <li id="menu-item-5461" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5461"><a href="pricing-tables/index.html"><span>Pricing Tables</span></a></li>
                                      </ul>
                                    </li>
                                    <li id="menu-item-5223" class="dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-5223"><a><span>Other Shortcodes</span></a>
                                      <ul class="sub-menu">
                                        <li id="menu-item-5447" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5447"><a href="content-slider/index.html"><span>Content Slider</span></a></li>
                                        <li id="menu-item-5460" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5460"><a href="post-type-shortcode/index.html"><span>Post Type Shortcode</span></a></li>
                                        <li id="menu-item-5441" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5441"><a href="statistics/index.html"><span>Statistics</span></a></li>
                                        <li id="menu-item-5440" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5440"><a href="contact-form-shortcode/index.html"><span>Contact Form Shortcode</span></a></li>
                                        <li id="menu-item-5451" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5451"><a href="lightboxes/index.html"><span>LightBoxes</span></a></li>
                                        <li id="menu-item-5452" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5452"><a href="media/index.html"><span>Media</span></a></li>
                                        <li id="menu-item-5445" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5445"><a href="audio-video-players/index.html"><span>Audio &#038; Video Players</span></a></li>
                                        <li id="menu-item-5455" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5455"><a href="google-maps/index.html"><span>Google Maps</span></a></li>
                                      </ul>
                                    </li>
                                  </ul>
                                </li>
                                <li id="menu-item-5218" class="dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-5218"><a><span>Layouts</span></a>
                                  <ul class="sub-menu">
                                    <li id="menu-item-5444" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5444"><a href="about-us/index.html"><span>About Us</span></a></li>
                                    <li id="menu-item-5474" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5474"><a href="services/index.html"><span>Services Page</span></a></li>
                                    <li id="menu-item-5435" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5435"><a href="contacts/index.html"><span>Contacts</span></a></li>
                                    <li id="menu-item-5215" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5215"><a href="indexf98b.html?p=99999"><span>404 Error Page</span></a></li>
                                    <li id="menu-item-5459" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5459"><a href="password-protected-page/index.html"><span>Password-protected page</span></a></li>
                                    <li id="menu-item-5463" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5463"><a href="sitemap/index.html"><span>Sitemap</span></a></li>
                                    <li id="menu-item-5458" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5458"><a href="left-sidebar/index.html"><span>Left Sidebar</span></a></li>
                                    <li id="menu-item-5462" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5462"><a href="right-sidebar/index.html"><span>Right Sidebar</span></a></li>
                                    <li id="menu-item-5467" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5467"><a href="timeline-portfolio/index.html"><span>Timeline Portfolio</span></a></li>
                                    <li id="menu-item-5466" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5466"><a href="timeline-blog/index.html"><span>Timeline Blog</span></a></li>
                                  </ul>
                                </li>
                                <li id="menu-item-5464" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5464"><a href="testimonials/index.html"><span>Testimonials</span></a></li>
                                <li id="menu-item-5220" class="dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-5220"><a><span>Heading Templates</span></a>
                                  <ul class="sub-menu">
                                    <li id="menu-item-5824" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5824"><a href="parallax-heading/index.html"><span>Parallax Heading</span></a></li>
                                    <li id="menu-item-5448" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5448"><a href="custom-heading/index.html"><span>Custom Heading Title</span></a></li>
                                    <li id="menu-item-5456" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5456"><a href="no-heading/index.html"><span>Heading with icon</span></a></li>
                                    <li id="menu-item-5457" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5457"><a href="heading-title-subtitle/index.html"><span>Heading With Title and Subtitle</span></a></li>
                                    <li id="menu-item-5439" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5439"><a href="heading-with-title-subtitle-icon/index.html"><span>Heading with Title, Subtitle &#038; Icon</span></a></li>
                                  </ul>
                                </li>
                                <li id="menu-item-5468" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5468"><a href="typography/index.html"><span>Typography</span></a></li>
                                <li id="menu-item-5438" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5438"><a href="widgets_sidebars/index.html"><span>Widgets and sidebars</span></a></li>
                                <li id="menu-item-5449" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5449"><a href="form-builder/index.html"><span>Form Builder</span></a></li>
                              </ul>
                            </li>
                            <li id="menu-item-5219" class="dropdown menu-item menu-item-type-custom menu-item-object-custom menu-item-5219"><a><span>Portfolio</span></a>
                              <ul class="sub-menu">
                                <li id="menu-item-5454" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5454"><a href="four-columns-portfolio/index.html"><span>Four Columns Portfolio</span></a></li>
                                <li id="menu-item-5465" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5465"><a href="three-columns-portfolio/index.html"><span>Three Columns Portfolio</span></a></li>
                              </ul>
                            </li>
                            <li id="menu-item-5434" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5434"><a href="blog/index.html"><span>Blog</span></a></li>
                            <li id="menu-item-5909" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5909"><a href="why-industrial/index.html"><span>Why Industrial?</span></a></li>-->
                          </ul>
                        </nav>
                        <div class="search_line">
                          <form method="get" action="http://industrial.cmsmasters.net/">
                            <p>
                              <input name="s" id="error_search" placeholder="enter keywords" value="" type="text">
                              <input value="" type="submit">
                            </p>
                          </form>
                        </div>
                        <ul class="social_icons">
                          <li> <a target="_blank" href="#" title="LinkedIn"> <img src="<?php echo get_template_directory_uri(); ?>/img/linkedin.png" alt="#" /> </a> <a class="cmsms_icon_title" href="#" title="LinkedIn">LinkedIn</a></li>
                          <li> <a target="_blank" href="#" title="Facebook"> <img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="#" /> </a> <a class="cmsms_icon_title" href="#" title="Facebook">Facebook</a></li>
                          <li> <a href="#" title="Google"> <img src="wp-content/uploads/2013/08/google-plus.png" alt="#" /> </a> <a class="cmsms_icon_title" href="#" title="Google">Google</a></li>
                          <li> <a target="_blank" href="#" title="RSS"> <img src="<?php echo get_template_directory_uri(); ?>/img/rss.png" alt="#" /> </a> <a class="cmsms_icon_title" href="#" title="RSS">RSS</a></li>
                          <li> <a href="#" title="Twitter"> <img src="wp-content/uploads/2013/08/twitter.png" alt="#" /> </a> <a class="cmsms_icon_title" href="#" title="Twitter">Twitter</a></li>
                        </ul>
                        <div class="cl"></div>
                        <!-- _________________________ Finish Navigation _________________________ --> 
                      </div>
                    </header>
			<!-- _________________________ Finish Header _________________________ --> 
    
            <!-- _________________________ Start Middle _________________________ -->
            <section id="middle" class=" cmsms_middle_margin"> 