<?php

function origin_widgets_add_admin_menu(){
	add_submenu_page('tools.php', __('Origin Widgets', 'siteorigin-panels-legacy-widgets'), __('Origin Widgets', 'siteorigin-panels-legacy-widgets'), 'edit_plugins', 'siteorigin-panels-legacy-widgets', 'origin_widgets_admin_page');
}
add_action('admin_menu', 'origin_widgets_add_admin_menu');

function origin_widgets_admin_page(){
	include plugin_dir_path(__FILE__).'tpl/main.php';
}