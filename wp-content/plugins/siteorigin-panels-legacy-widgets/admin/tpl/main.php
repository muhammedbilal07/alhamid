<div class="wrap">
	<div id="icon-tools" class="icon32"><br></div>
	<h2><?php _e('Origin Widgets', 'siteorigin-panels-legacy-widgets') ?></h2>

	<?php foreach(origin_widgets_get_widgets() as $widget) : ?>
		<li><?php echo trim(str_replace('(Origin)', '', $widget->name)) ?></li>
	<?php endforeach; ?>

</div>