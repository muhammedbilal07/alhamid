<?php
/*
Plugin Name: Page Builder Legacy Widgets
Plugin URI: http://siteorigin.com/siteorigin-panels-legacy-widgets/
Description: A few common website elements. Designed to be used with Page Builder. Widgets include button, call to action, list, price box, testimonial, headline, progress bar, etc.
Version: 1.0
Author: Greg Priday
Author URI: http://siteorigin.com/
License: GPL3
License URI: http://www.gnu.org/licenses/gpl.html
Text Domain: siteorigin-panels-legacy-widgets
*/

define( 'SITEORIGIN_PANELS_LEGACY_WIDGETS_ACTIVE', true );
define( 'SITEORIGIN_PANELS_LEGACY_WIDGETS_VERSION', '1.0' );
define( 'SITEORIGIN_PANELS_LEGACY_WIDGETS_BASE_FILE', __FILE__ );

// Include all the basic widgets
include plugin_dir_path( __FILE__ ) . '/admin/admin.php';
include plugin_dir_path( __FILE__ ) . '/less/functions.php';
include plugin_dir_path( __FILE__ ) . '/widgets/widget.class.php';

function origin_widgets_init(){
	if( !defined('SITEORIGIN_LEGACY_WIDGETS_INLINE_CSS') ) {
		define('SITEORIGIN_LEGACY_WIDGETS_INLINE_CSS', true);
	}
}
add_action('init', 'origin_widgets_init');

/**
 * Include all the widget files and register their widgets
 */
function origin_widgets_widgets_init(){

	foreach(glob(plugin_dir_path(__FILE__).'/widgets/*/*.php') as $file) {
		include_once $file;

		$p = pathinfo($file);
		$class = $p['filename'];
		$class = str_replace('-', ' ', $class);
		$class = ucwords($class);
		$class = str_replace(' ', '_', $class);

		$class = 'SiteOrigin_Panels_Widget_'.$class;
		if(class_exists($class)) register_widget($class);
	}

}
add_action('widgets_init', 'origin_widgets_widgets_init');

function origin_widgets_get_widgets(){
	$widgets = array();

	foreach(glob(plugin_dir_path(__FILE__).'/widgets/*/*.php') as $file) {
		include_once $file;

		$p = pathinfo($file);
		$class = $p['filename'];
		$class = str_replace('-', ' ', $class);
		$class = ucwords($class);
		$class = str_replace(' ', '_', $class);

		$class = 'Origin_Widget_'.$class;
		if(class_exists($class)) {
			$widgets[$p['filename']] = new $class();
		}
	}

	return $widgets;
}

/**
 * Display the CSS on a request to the index page.
 */
function origin_widgets_display_css(){
	if(is_admin()) return;
	if(empty($_GET['action']) || $_GET['action'] != 'origin_widgets_css') return;
	if(empty($_GET['class']) || empty($_GET['style']) || empty($_GET['preset'])) return;
	if(strpos($_GET['class'], 'Origin_Widget_') !== 0) return;

	header("Content-type: text/css");
	echo origin_widgets_generate_css($_GET['class'], $_GET['style'], $_GET['preset'], $_GET['ver']);
	exit();
}
add_action('init', 'origin_widgets_display_css');

/**
 * Generate CSS
 *
 * @param $class
 * @param $style
 * @param $preset
 * @param null $version
 * @return mixed|string
 */
function origin_widgets_generate_css($class, $style, $preset, $version = null){
	$widget = new $class();
	if( !is_subclass_of($widget, 'Origin_Widget') ) return '';
	if(empty($version)) $version = ORIGIN_WIDGETS_VERSION;

	$id = str_replace('_', '', strtolower(str_replace('Origin_Widget_', '', $class)));
	$key = strtolower($id.'-'.$style.'-'. $preset.'-'.str_replace('.', '', $version));

	$css = get_site_transient('origin_wcss:'.$key);
	if($css === false || ( defined('ORIGIN_WIDGETS_NOCACHE') && ORIGIN_WIDGETS_NOCACHE ) ) {

		echo "/* Regenerate Cache */\n\n";
		// Recreate and minify the CSS
		$css = $widget->create_css($style, $preset);
		$css = preg_replace('#/\*.*?\*/#s', '', $css);
		$css = preg_replace('/\s*([{}|:;,])\s+/', '$1', $css);
		$css = preg_replace('/\s\s+(.*)/', '$1', $css);
		$css = str_replace(';}', '}', $css);

		set_site_transient('origin_wcss:'.$key, $css, 86400);
	}

	return $css;
}