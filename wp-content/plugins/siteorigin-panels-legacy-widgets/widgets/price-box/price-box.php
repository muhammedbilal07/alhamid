<?php

class SiteOrigin_Panels_Widget_Price_Box extends SiteOrigin_Panels_Legacy_Widget  {
	function __construct() {
		parent::__construct(
			__('Price Box (Origin)', 'siteorigin-panels-legacy-widgets'),
			array(
				'description' => __('Displays a bullet list of elements', 'siteorigin-panels-legacy-widgets'),
				'default_style' => 'simple',
			),
			array(),
			array(
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'siteorigin-panels-legacy-widgets'),
				),
				'price' => array(
					'type' => 'text',
					'label' => __('Price', 'siteorigin-panels-legacy-widgets'),
				),
				'per' => array(
					'type' => 'text',
					'label' => __('Per', 'siteorigin-panels-legacy-widgets'),
				),
				'information' => array(
					'type' => 'text',
					'label' => __('Information Text', 'siteorigin-panels-legacy-widgets'),
				),
				'features' => array(
					'type' => 'textarea',
					'label' => __('Features Text', 'siteorigin-panels-legacy-widgets'),
					'description' => __('Start each new point with an asterisk (*)', 'siteorigin-panels-legacy-widgets'),
				),
				'button_text' => array(
					'type' => 'text',
					'label' => __('Button Text', 'siteorigin-panels-legacy-widgets'),
				),
				'button_url' => array(
					'type' => 'text',
					'label' => __('Button URL', 'siteorigin-panels-legacy-widgets'),
				),
				'button_new_window' => array(
					'type' => 'checkbox',
					'label' => __('Open In New Window', 'siteorigin-panels-legacy-widgets'),
				),
			)
		);

		$this->add_sub_widget('button', __('Button', 'siteorigin-panels-legacy-widgets'), 'SiteOrigin_Panels_Widget_Button');
		$this->add_sub_widget('list', __('Feature List', 'siteorigin-panels-legacy-widgets'), 'SiteOrigin_Panels_Widget_List');
	}
}