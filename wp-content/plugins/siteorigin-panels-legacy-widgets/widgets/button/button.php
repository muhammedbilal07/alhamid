<?php

class SiteOrigin_Panels_Widget_Button extends SiteOrigin_Panels_Legacy_Widget  {
	function __construct() {
		parent::__construct(
			__('Button (Origin)', 'siteorigin-panels-legacy-widgets'),
			array(
				'description' => __('A simple button', 'siteorigin-panels-legacy-widgets'),
				'default_style' => 'simple',
			),
			array(),
			array(
				'text' => array(
					'type' => 'text',
					'label' => __('Text', 'siteorigin-panels-legacy-widgets'),
				),
				'url' => array(
					'type' => 'text',
					'label' => __('Destination URL', 'siteorigin-panels-legacy-widgets'),
				),
				'new_window' => array(
					'type' => 'checkbox',
					'label' => __('Open In New Window', 'siteorigin-panels-legacy-widgets'),
				),
				'align' => array(
					'type' => 'select',
					'label' => __('Button Alignment', 'siteorigin-panels-legacy-widgets'),
					'options' => array(
						'left' => __('Left', 'siteorigin-panels-legacy-widgets'),
						'right' => __('Right', 'siteorigin-panels-legacy-widgets'),
						'center' => __('Center', 'siteorigin-panels-legacy-widgets'),
						'justify' => __('Justify', 'siteorigin-panels-legacy-widgets'),
					)
				),
			)
		);
	}

	function widget_classes($classes, $instance) {
		$classes[] = 'align-'.(empty($instance['align']) ? 'none' : $instance['align']);
		return $classes;
	}
}