<?php

class SiteOrigin_Panels_Widget_Call_To_Action extends SiteOrigin_Panels_Legacy_Widget  {
	function __construct() {
		parent::__construct(
			__('Call To Action (Origin)', 'siteorigin-panels-legacy-widgets'),
			array(
				'description' => __('A Call to Action block', 'siteorigin-panels-legacy-widgets'),
				'default_style' => 'simple',
			),
			array(),
			array(
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'siteorigin-panels-legacy-widgets'),
				),
				'subtitle' => array(
					'type' => 'text',
					'label' => __('Sub Title', 'siteorigin-panels-legacy-widgets'),
				),
				'button_text' => array(
					'type' => 'text',
					'label' => __('Button Text', 'siteorigin-panels-legacy-widgets'),
				),
				'button_url' => array(
					'type' => 'text',
					'label' => __('Button URL', 'siteorigin-panels-legacy-widgets'),
				),
				'button_new_window' => array(
					'type' => 'checkbox',
					'label' => __('Open In New Window', 'siteorigin-panels-legacy-widgets'),
				),
			)
		);

		// We need the button style
		$this->add_sub_widget('button', __('Button', 'siteorigin-panels-legacy-widgets'), 'SiteOrigin_Panels_Widget_Button');
	}
}