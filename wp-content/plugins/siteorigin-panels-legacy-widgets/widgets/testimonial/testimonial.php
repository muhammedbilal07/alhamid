<?php

class SiteOrigin_Panels_Widget_Testimonial extends SiteOrigin_Panels_Legacy_Widget  {
	function __construct() {
		parent::__construct(
			__('Testimonial (Origin)', 'siteorigin-panels-legacy-widgets'),
			array(
				'description' => __('Displays a bullet list of elements', 'siteorigin-panels-legacy-widgets'),
				'default_style' => 'simple',
			),
			array(),
			array(
				'name' => array(
					'type' => 'text',
					'label' => __('Name', 'siteorigin-panels-legacy-widgets'),
				),
				'location' => array(
					'type' => 'text',
					'label' => __('Location', 'siteorigin-panels-legacy-widgets'),
				),
				'image' => array(
					'type' => 'text',
					'label' => __('Image', 'siteorigin-panels-legacy-widgets'),
				),
				'text' => array(
					'type' => 'textarea',
					'label' => __('Text', 'siteorigin-panels-legacy-widgets'),
				),
				'url' => array(
					'type' => 'text',
					'label' => __('URL', 'siteorigin-panels-legacy-widgets'),
				),
				'new_window' => array(
					'type' => 'checkbox',
					'label' => __('Open In New Window', 'siteorigin-panels-legacy-widgets'),
				),
			)
		);
	}
}