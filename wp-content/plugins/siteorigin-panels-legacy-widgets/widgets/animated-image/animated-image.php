<?php

class SiteOrigin_Panels_Widget_Animated_Image extends SiteOrigin_Panels_Legacy_Widget {
	function __construct() {
		parent::__construct(
			__('Animated Image (Origin)', 'siteorigin-panels-legacy-widgets'),
			array(
				'description' => __('An image that animates in when it enters the screen.', 'siteorigin-panels-legacy-widgets'),
				'default_style' => 'simple',
			),
			array(),
			array(
				'image' => array(
					'type' => 'text',
					'label' => __('Image URL', 'siteorigin-panels-legacy-widgets'),
				),
				'animation' => array(
					'type' => 'select',
					'label' => __('Animation', 'siteorigin-panels-legacy-widgets'),
					'options' => array(
						'fade' => __('Fade In', 'siteorigin-panels-legacy-widgets'),
						'slide-up' => __('Slide Up', 'siteorigin-panels-legacy-widgets'),
						'slide-down' => __('Slide Down', 'siteorigin-panels-legacy-widgets'),
						'slide-left' => __('Slide Left', 'siteorigin-panels-legacy-widgets'),
						'slide-right' => __('Slide Right', 'siteorigin-panels-legacy-widgets'),
					)
				),
			)
		);
	}

	function enqueue_scripts(){
		static $enqueued = false;
		if(!$enqueued) {
			wp_enqueue_script('siteorigin-panels-legacy-widgets-'.$this->origin_id.'-onscreen', plugin_dir_url(__FILE__).'js/onscreen.js', array('jquery'), ORIGIN_WIDGETS_VERSION);
			wp_enqueue_script('siteorigin-panels-legacy-widgets-'.$this->origin_id, plugin_dir_url(__FILE__).'js/main.js', array('jquery'), ORIGIN_WIDGETS_VERSION);
			$enqueued = true;
		}

	}
}